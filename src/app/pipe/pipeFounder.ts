import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'pipeFounder'
  })
  export class PipeFounder implements PipeTransform {
    isFounder:string; 
    transform(value: number): string {
        if(value == 1){
            this.isFounder = "unique";
        }
        else if(value == 2){
            this.isFounder = "couple";
        }else if(value > 2){
            this.isFounder = "groupe";
        }
      return this.isFounder;
    }
  }
  