import { Injectable } from '@angular/core';
import { Startup } from './startup';
import { Consultant } from './consultant';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  constructor() { }

  createDb() {
    const startups = [
      new Startup(1, 'Ma startup', 'Web', 'Tony', 1, 'Description de ma startup', 'rue pierre de maupertuis Bruz'),
      new Startup(2, 'Autre startup', 'SSII', 'Valentin', 2, "Description de l'autre startup", 'rue pierre de maupertuis Bruz')     
     ];

     const consultants = [
      new Consultant(1, 'Tony', 'Tosarelli', 'Description du consultant'),
      new Consultant(2, 'Valentin', 'Andre', 'Description du consultant')     
     ];

    return { startups, consultants };
  }

  genId(startups: Startup[]): number {
    return startups.length > 0 ? Math.max(...startups.map(startup => startup.id)) + 1 : 11;
  }
}
