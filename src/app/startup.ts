export class Startup {

    id: number;
    nom: string;
    secteur:string;
    nomRepresentant:string;
    nb_cofounder:number;
    description:string;
    adresse:string;

    constructor(id, nom, secteur, nomRepresentant, nb_cofounder , description, adresse ) {
        this.id = id;
        this.nom = nom; 
        this.secteur = secteur;
        this.nomRepresentant = nomRepresentant;
        this.nb_cofounder = nb_cofounder; 
        this.description = description;  
        this.adresse = adresse;       
    }
}