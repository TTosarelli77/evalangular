import { Component, OnInit, Input } from '@angular/core';
import { Startup } from '../startup';

@Component({
  selector: 'app-loop-startup',
  templateUrl: './loop-startup.component.html',
  styleUrls: ['./loop-startup.component.css']
})
export class LoopStartupComponent implements OnInit {

  constructor() { }
  
  @Input() tableauStartups: Startup[];
 
  ngOnInit() {
  }

}
