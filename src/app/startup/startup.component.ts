import { Component, OnInit, Directive } from '@angular/core';
import { StartupService } from '../startup.service';
import { Startup } from '../startup';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-startup',
  templateUrl: './startup.component.html',
  styleUrls: ['./startup.component.css']
})

@Directive({
  selector: 'loopStartup'
})

export class StartupComponent implements OnInit {

  tableauStartup:Startup[];
  startup: Startup;

  nom: FormControl;
  secteur: FormControl;
  nomDurepresentant: FormControl;
  cofounder: FormControl;
  description: FormControl;
  adresse: FormControl;

  startupForm: FormGroup;

  constructor(fb: FormBuilder,private startupService:StartupService,private http:HttpClient) {
    /*this.nom = fb.control('',[Validators.required, Validators.maxLength(20)]);
    this.secteur = fb.control('',[Validators.required, Validators.maxLength(10)]);
    this.nomDurepresentant = fb.control('', [Validators.required, Validators.maxLength(15)]);
    this.cofounder = fb.control('', Validators.required);
    this.description = fb.control('',[Validators.required, Validators.maxLength(250)]);
    this.adresse = fb.control('', Validators.maxLength(25));

    this.startupForm = fb.group({
      nom: this.nom,
      secteur: this.secteur,
      nomDurepresentant: this.nomDurepresentant,
      cofounder: this.cofounder,
      description: this.description,
      adresse: this.adresse
    });*/

   }

  ngOnInit() {
    this.startupService.list()
      .subscribe(
        (x: Array<Startup>) => this.tableauStartup = x,
        () => console.log(this.tableauStartup)
      );
  }

  hide(){

  }

}
