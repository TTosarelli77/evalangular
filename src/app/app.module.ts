import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { StartupComponent } from './startup/startup.component';
import { NsTableComponent } from './ns-table/ns-table.component';
import { LoopStartupComponent } from './loop-startup/loop-startup.component';
import { RowStartupComponent } from './row-startup/row-startup.component';
import { PipeFounder } from './pipe/pipeFounder';

@NgModule({
  declarations: [
    AppComponent,
    StartupComponent,
    NsTableComponent,
    LoopStartupComponent,
    RowStartupComponent,
    PipeFounder
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }),
    NgZorroAntdModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
