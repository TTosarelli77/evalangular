import { Component, OnInit, Input } from '@angular/core';
import { Startup } from '../startup';
import { StartupService } from '../startup.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-row-startup',
  templateUrl: './row-startup.component.html',
  styleUrls: ['./row-startup.component.css']
})
export class RowStartupComponent implements OnInit {
  @Input() startup: Startup;

  @Input() tableauStartups: Startup[];

  constructor(private startupService: StartupService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.startup;
  }

  delete(startup) {
    this.startupService.delete(startup.id).subscribe(
      x => this.tableauStartups.splice(startup,1),
      err => console.log(err),
      () => console.log('Observer got a complete notification')
    );
    //this.notificationService.add(prenom + ' ' + nom + ' supprimé !');
  }
}
