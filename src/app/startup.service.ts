import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Startup } from './startup';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StartupService {
  
  startup: Observable<Startup>;

  constructor(private http:HttpClient) { }

  list() {
    return this.http.get('http://toto/api/startups'); 
  }

  delete(id){
    return this.http.delete(`http://toto/api/startups/${id}`);
  }

}
