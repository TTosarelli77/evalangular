export class Consultant {

    id: number;
    nom: string;
    prenom:string;
    description:string;

    constructor(id, nom, prenom , description ) {
        this.id = id;
        this.nom = nom; 
        this.prenom = prenom; 
        this.description = description;  
    }
}